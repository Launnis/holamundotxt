# Apuntes sobre cómo hacer push en la rama main

- Por un lado he añadido la clave *ssh*
- Pero me sigue sin dejar por lo tanto en vez de hacerlo con *git remote add origin* + **la url**, vamos a hacerlo clonando el repositorio en nuestro ordenador con **git clone** + **clave ssh**
- Una vez hecho eso ya nos deja hacer push en la rama *main*. 
- Si lo queremos hacer de la otra forma vamos a tener que utilizar otra rama.

